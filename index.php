<?php

include_once 'simple_html_dom.php';


/**
 * Parser information by bulbs for automotive from putco.com
 */

class PutcoMmyBulbParser
{
    const BASE_URL = 'https://www.putco.com/';
    const RESULT_DIR = 'results';

    private $dropdownIdLabels = [
        '11' => 'Year',
        '12' => 'Make',
        '13' => 'Model',
        '14' => 'Submodel',
        '15' => 'Fitment Note',
    ];

    /**
     * Generate path for save files
     * @param string $path
     * @return string
     */
    private function getResultDir($path = '') {
        return __DIR__.DIRECTORY_SEPARATOR.self::RESULT_DIR.DIRECTORY_SEPARATOR . ($path ? $path : '');
    }

    /**
     * Save data to json
     * @param array $data
     */
    private function saveToJson($data) {
        file_put_contents($this->getResultDir($data['last'].'.json'), json_encode($data));
    }

    /**
     * Parsing and analyze data from
     * @param int $dropdownId
     * @param int $parentId
     * @param array $data
     */
    private function parseMMY($dropdownId, $parentId, $data = []) {
        $jsonContents = file_get_contents(self::BASE_URL.'vehicle/index/options/?dropdown_id='.$dropdownId.'&parent_id='.$parentId);

        if (!$jsonContents) {
            die('bad json contents');
        }
        $data['last'] = $parentId;
        $json = json_decode($jsonContents, true);

        if (!count($json) || (count($json)==1 && $dropdownId>=13 )) {
            $html = file_get_html('https://www.putco.com/vehicle/find/-'.$parentId);
            $lightingItems = $html->find('.lighting-item-link');
            foreach($lightingItems AS $item) {
                $bulbClass = $item->find('.lighting-bulb', 0);
                $bulb = trim($bulbClass->plaintext);
                $bulbClass->outertext = '';
                $itemHtml = str_get_html($item->innertext);
                $data['bulbs'][] = [
                    'name' => trim($itemHtml->plaintext),
                    'type' => $bulb
                ];
            }
            $this->saveToJson($data);

        } else {
            foreach($json AS $val) {
                $data[$this->dropdownIdLabels[$dropdownId]] = $val['label'];
                $data[$this->dropdownIdLabels[$dropdownId].'-value'] = $val['value'];
                $this->parseMMY($dropdownId+1, $val['value'], $data);
            }
        }
    }

    /**
     * Running initial actions
     */
    function run(){
        /*
         * Clear previous data
         */
        $dir = $this->getResultDir();
        if (!file_exists($dir)) {
            mkdir($dir);
        }

        foreach(scandir($dir) AS $dirFile) {
            if ('.' == $dirFile || '..' == $dirFile || !is_file($dir.$dirFile)) {
                continue;
            }
            unlink($dir.$dirFile);
        }

        /*
         * Parse initial years
         */
        $html = file_get_html(self::BASE_URL);
        $dataDropdownOptions = $html->find('select[data-dropdown-id=11] option');
        foreach($dataDropdownOptions AS $option) {
            $year = trim($option->plaintext);
            if (!$year || false !== strpos($year, 'Select')  ) {
                continue;
            }
            $this->parseMMY(12, $option->value, [$this->dropdownIdLabels[11] => $year, 'last'=>$option->value]);
        }
    }
}


// Run parser
$parser = new PutcoMmyBulbParser();
$parser->run();

